<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "diary".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $title
 * @property string $note
 * @property string $date_event
 * @property string $date_taken
 * @property string $status
 *
 * @property User $user
 */
class Diary extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'diary';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'title', 'note', 'date_event', 'date_taken', 'status'], 'required'],
            [['user_id'], 'integer'],
            [['note'], 'string'],
            [['date_event', 'date_taken'], 'safe'],
            [['title'], 'string', 'max' => 200],
            [['status'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'title' => 'Title',
            'note' => 'Note',
            'date_event' => 'Date Event',
            'date_taken' => 'Date Taken',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
