<?php
namespace app\models;

use yii\base\Model;
use Yii;
use app\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $Name;
    public $username;
    public $email;
    public $contact;
    public $password;
    public $verifyCode;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['Name','required'],
            ['Name', 'match', 'pattern' => '/^[a-zA-Z\s]+$/','message'=>'Name must contain only Alphabets'],
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['email','required'],
            ['email','email'],
            ['email', 'unique', 'targetClass' => 'app\models\User', 'message' => 'This email has already been taken.'],
            ['username', 'unique', 'targetClass' => 'app\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['contact','required'],
            ['contact','integer'],
            
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->Name = $this->Name;
            $user->username = $this->username;;
            $user->email = $this->email;
            $user->contact = $this->contact;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            
            $value = Yii::$app->mailer->compose()
                    ->setFrom(['support@technospider.zz.mu' => 'E-diary'])
                    ->setTo($this->email)
                    ->setSubject('Welcome')
                    ->setHtmlBody('Welcome to the E-diary Arena. Store your important reminders here.')
                    ->send();
            $user->save();        
            return $user;
        }
    }
}
