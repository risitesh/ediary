<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Diary */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="diary-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 200]) ?>

    <?= $form->field($model, 'note')->textarea(['rows' => 6]) ?>
    
    <?= $form->field($model, 'date_event')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Date Of Event'],
        'pluginOptions' => [
            'autoclose'=>true,
            'format'=>'yyyy-mm-dd',
        ]
        ]);
    ?>

    <?= $form->field($model, 'status')->dropDownList([''=>'Select Status','Public'=>'Public','Private'=>'Private'])->label('Status') ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
