<?php
/* @var $this yii\web\View */
$this->title = 'E-diary';
?>
<div class="site-index">
    <div class="container">
        <div class="well">
            <h1 style="text-align: center;font-family: Arial;color: #006aeb">Welcome To E-diary</h1>
        </div>
        <div class="well">
            <p><ul style="font-size: 20px"><li>Everyone can have their personal online or e-diary.</li><li>So store your events or activities here.</li><li>It also has the facility of public & private by which you can choose whether to show it to public or not.</li><li>If you have any queries contact us on the Contact Us Page.</li></ul></p>
        </div>
    </div>
</div>
